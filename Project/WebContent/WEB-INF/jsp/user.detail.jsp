<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ情報</title>
    <link rel="stylesheet" href="css/detail.css">
</head>
<body>
    <header class="row">
        <p>${userInfo.name}さん</p>
        <a class="rogout" href="LogoutServlet">ログアウト</a>
    </header>

    <div class="copy-container">
    <h1>ユーザ情報詳細参照</h1>
    </div>


    <div class="contents">
        <div class="id">
            <p>ログインID</p>
			<p>${user.loginId}</p>
        </div>

        <div class="userName">
            <p>ユーザ名</p>
            <p>${user.name}</p>

        </div>

        <div class="birth">
            <p>生年月日</p>
            <p>${user.birthDate}</p>
        </div>

        <div class="entry_time">
            <p>登録日時 </p>
            <p>${user.createDate}</p>
        </div>

         <div class="new_time">
             <p>更新日時</p>
             <p>${user.updateDate}</p>
        </div>
    </div>

        <a href="ListServlet">戻る</a>
</body>
</html>