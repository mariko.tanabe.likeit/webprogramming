<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザー削除確認</title>
    <link rel="stylesheet" href="css/delete.css">
</head>
<body>
    <header>
        <p>${userInfo.name}さん</p>
        <a class="rogout" href="LogoutServlet">ログアウト</a>
    </header>
    <div class="copy-container">
        <h1>ユーザー削除確認</h1>
    </div>
	<form action="DeleteServlet" method="post">
		<input type="hidden" value="${user.id}" name="id">
    <div class="contents">
        <p>ログインID: ${user.name}</p>
        <p>を本当に削除してよろしいでしょうか。</p>
    </div>

    <div class="botton">
        <input type="submit" class="okButton" value="OK">
        <a href="ListServlet" ><input type="button" value="キャンセル"></a>
     </div>
	</form>
</body>
</html>