<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ情報更新</title>
    <link rel="stylesheet" href="css/entry.css">
</head>
<body>
    <header>
        <p>${userInfo.name}さん</p>
        <a class="rogout" href="LogoutServlet">ログアウト</a>
    </header>

    <div class="copy-container">
    <h1>ユーザ情報更新</h1>
    </div>
	<c:if  test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

	<form  action="RenewalServlet" method="post">
        <div class="form-group">
            <p>ログインID</p>
            <p>${user.loginId}</p>
        </div>

		<input type="hidden" value="${user.id}" name="id">

        <div class="form-group">
            <label for="InputPassword1">パスワード</label>
            <input type="password" name="password1" id="password1" placeholder="パスワード">
        </div>

        <div class="form-group">
            <label for="InputPassword1">パスワード(確認)</label>
            <input type="password" name="password2" id="password2" placeholder="パスワード(確認)">
        </div>

        <div class="form-group">
            <label for="InputPassword1">ユーザ名</label>
            <input type="text" name="name" id="name" value="${user.name}">
        </div>

         <div class="form-group">
            <label for="InputPassword1">生年月日</label>
            <input type="text" name="birth" id="birth" value="${user.birthDate}">
        </div>
    <div>
        <input type="submit" value="更新">
    </div>
    </form>
        <a href="ListServlet">戻る</a>
</body>
</html>