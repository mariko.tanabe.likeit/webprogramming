<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="styleseet" href="css/login.css">
</head>
<body>
	<header>

	<h1>ログイン画面</h1>



	</header>

	<div class="container">

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<form class="form-signin" action="LoginServlet" method="post">
			 <input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインID" required autofocus>
			 <input type="password" name="password" id="inputPassword" class="form-control" placeholder="パスワード" required>
			<button type="submit">ログイン</button>
		</form>



	</div>
</body>
</html>