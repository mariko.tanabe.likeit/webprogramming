<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link rel="stylesheet" href="css/list.css">
</head>

<body>
	<header>
		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text">${userInfo.name}さん</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a></li>
		</ul>
	</header>

	<div class="container">


			<div class="entry">
				<c:if test="${userInfo.loginId.equals('admin')}"><a href="EntryServlet" >新規登録</a></c:if>
			</div>
		<form action="ListServlet" method="post">
			<div class="loginID">
				<p>ログインID:</p>
				<input type="text" name="login-id" id="login-id">
			</div>

			<div class="user_name">
				<p>ユーザー名:</p>
				<input type="text" name="user-name" id="user-name">
			</div>

			<div class="BirthDay">
				<label for="continent">生年月日</label>
				<div class="row">
					<div class="col-sm-2">
						<input type="date" name="date-start" id="date-start"
							class="form-control" size="30" />
					</div>
					<div class="col-xs-1 text-center">~</div>
					<div class="col-sm-2">
						<input type="date" name="date-end" id="date-end"
							class="form-control" />
					</div>
				</div>
			</div>

			<div class="botton">
				<input type="submit" class="Button" value="検索">
			</div>
		</form>
	</div>

	<div class="membersList">
		<table class="table" border="3">
			<thead>
				<tr>
					<th>ログインID</th>
					<th>ユーザ名</th>
					<th>生年月日</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${userList}">
					<tr>
						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<td>
							<div class="btn-group" role="group" aria-label="Basic example">


								<a href="DetailServlet?id=${user.id}">
									<button type="button" class="btn1" >詳細</button>
								</a>

								 <a href="RenewalServlet?id=${user.id}">
									<c:if test="${userInfo.name.equals('管理者') or userInfo.loginId.equals(user.loginId)}"><button type="button" class="btn2">更新</button></c:if>
								</a>

								<a href="DeleteServlet?id=${user.id}">
									<c:if test="${userInfo.loginId.equals('admin')}"><button type="button" class="btn3">削除</button></c:if>
								</a>
							</div>
						</td>
					</tr>

				</c:forEach>
			</tbody>
		</table>
	</div>



</body>
</html>