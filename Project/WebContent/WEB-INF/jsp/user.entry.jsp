<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ新規登録</title>
    <link rel="stylesheet" href="css/entry.css">
</head>
<body>
    <header>
        <p>${userInfo.name}さん</p>
        <a class="rogout" href="LogoutServlet">ログアウト</a>
    </header>

    <div class="copy-container">
        <h1>ユーザ新規登録</h1>
    </div>

    <c:if  test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>


	<form action="EntryServlet" method="post">
        <div class="loginId">
            <p>ログインID</p>
            <input type="text"  name="loginId" id="loginId"  placeholder="ID">

        </div>

        <div class="password1">
            <p>パスワード</p>
            <input type="text" name="password1" id="password1" placeholder="パスワード">
        </div>

        <div class="password2">
            <p>パスワード(確認)</p>
            <input type="text" name="password2" id="password2" placeholder="パスワード(確認)">
        </div>

        <div class="name">
            <p>ユーザ名</p>
            <input type="text" name="name" id="name" placeholder="ユーザ名">
        </div>

         <div class="birth">
            <p>生年月日</p>
            <input type="date" name="birth" id="birth" placeholder="生年月日">
        </div>
    <div>
        <input type="submit" value="登録">
    </div>
    </form>
        <a href="ListServlet">戻る</a>
</body>
</html>