package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class RenewalServlet
 */
@WebServlet("/RenewalServlet")
public class RenewalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RenewalServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session=request.getSession(false);
		if(session==null) {
			session=request.getSession(true);
			response.sendRedirect("/WEB-INF/jsp/login.jsp");
		}

		String id=request.getParameter("id");
		System.out.println(id);
		UserDao userDao=new UserDao();
		User user=userDao.userDetail(id);
		request.setAttribute("user", user);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/user.renewal.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		Connection conn=null;

		String id=request.getParameter("id");

		String password=request.getParameter("password1");
		String password2=request.getParameter("password2");
		String name=request.getParameter("name");
		String birthDate=request.getParameter("birth");
		UserDao userDao=new UserDao();


		try {
			if(name.isEmpty()||birthDate.isEmpty()) {
				request.setAttribute("errMsg", "入力された内容は正しくありません。");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/user.renewal.jsp");
				dispatcher.forward(request, response);
				return;

			}else if(!(password.equals(password2))){
				request.setAttribute("errMsg", "入力された内容は正しくありません。");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/user.renewal.jsp");
				dispatcher.forward(request, response);
				return;

			}
			if(password.isEmpty()&& password2.isEmpty()) {
				userDao.userUpdate2(name,birthDate,id);
				response.sendRedirect("ListServlet");
			}else if(password.isEmpty()|| password2.isEmpty()){
				request.setAttribute("errMsg", "入力された内容は正しくありません。");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/user.renewal.jsp");
				dispatcher.forward(request, response);
				return;
			}else {
				String result=userDao.Code(password);
				userDao.userUpdate(result,name,birthDate,id);
				response.sendRedirect("ListServlet");
			}
		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
