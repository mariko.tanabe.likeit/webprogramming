package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

/**
 * Servlet implementation class EntryServlet
 */
@WebServlet("/EntryServlet")
public class EntryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EntryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/user.entry.jsp");
		dispatcher.forward(request, response);

		HttpSession session=request.getSession(false);
		if(session==null) {
			session=request.getSession(true);
			response.sendRedirect("/WEB-INF/jsp/login.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		Connection conn=null;
		String loginID=request.getParameter("loginId");
		String password=request.getParameter("password1");
		String password2=request.getParameter("password2");
		String name=request.getParameter("name");
		String birthDate=request.getParameter("birth");
		UserDao userDao=new UserDao();
		try {
			if(loginID.isEmpty()|| password.isEmpty()||password2.isEmpty()||name.isEmpty()||birthDate.isEmpty()) {
				request.setAttribute("errMsg", "入力された内容は正しくありません。");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/user.entry.jsp");
				dispatcher.forward(request, response);
				return;

			}else if(!(password.equals(password2))){
				request.setAttribute("errMsg", "入力された内容は正しくありません。");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/user.entry.jsp");
				dispatcher.forward(request, response);
				return;

			}else if(userDao.findById(loginID)!=null){
				request.setAttribute("errMsg", "入力された内容は正しくありません。");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/user.entry.jsp");
				dispatcher.forward(request, response);
				return;

			}

			String result=userDao.Code(password);
			userDao.userEntry(loginID,result,name,birthDate);
			response.sendRedirect("ListServlet");

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
