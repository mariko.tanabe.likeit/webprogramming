package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class ListServlet
 */
@WebServlet("/ListServlet")
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session=request.getSession(false);
		if(session==null) {
			session=request.getSession(true);
			response.sendRedirect("/WEB-INF/jsp/login.jsp");
		}


		UserDao userDao=new UserDao();
		List<User> userList=userDao.findAll();

		request.setAttribute("userList", userList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.list.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId=request.getParameter("login-id");
		String name=request.getParameter("user-name");
		String birthData=request.getParameter("date-start");
		String endData=request.getParameter("date-end");
		UserDao userDao=new UserDao();

		List<User> serchList=userDao.serch(loginId, name, birthData,endData);

		request.setAttribute("userList", serchList);
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/user.list.jsp");
		dispatcher.forward(request, response);

	}

}
