package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public String Code(String password){
		String source = password;
		String result="";
		Charset charset = StandardCharsets.UTF_8;

		String algorithm = "MD5";


		byte[] bytes;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

			result = DatatypeConverter.printHexBinary(bytes);

			System.out.println(result);

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return result;

	}

	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql="SELECT * FROM user WHERE login_id =? and password =?";
			PreparedStatement pStmt=conn.prepareStatement(sql);

			pStmt.setString(1,loginId);
			pStmt.setString(2, password);
			ResultSet rs=pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			String loginIdData=rs.getString("login_id");
			String nameData=rs.getString("name");
			return new User(loginIdData,nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll(){
		Connection conn=null;
		List<User> userList=new ArrayList<User>();
		try {
			conn=DBManager.getConnection();
			String sql="SELECT*FROM user WHERE name != '管理者'";

			Statement stmt=conn.createStatement();
			ResultSet rs=stmt.executeQuery(sql);

			while(rs.next()) {
				int id=rs.getInt("id");
				String loginId=rs.getString("login_id");
				String name=rs.getString("name");
				Date birthDate=rs.getDate("birth_date");
				String password=rs.getString("password");
				String createDate=rs.getString("create_date");
				String updateDate=rs.getString("update_date");
				User user=new User(id,loginId,name,birthDate,password,createDate,updateDate);

				userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public User userDetail(String id) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			String sql="SELECT * FROM user WHERE id=?";
			PreparedStatement pStmt=conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs=pStmt.executeQuery();

			while(rs.next()) {
				int ID=rs.getInt("id");
				String loginId=rs.getString("login_id");
				String name=rs.getString("name");
				Date birthDate=rs.getDate("birth_date");
				String password=rs.getString("password");
				String createDate=rs.getString("create_date");
				String updateDate=rs.getString("update_date");
				User user=new User(ID,loginId,name,birthDate,password,createDate,updateDate);

				return user;
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public void userEntry(String loginId,String password,String name,String birthDate) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="INSERT INTO user (login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now())";
			pstmt=conn.prepareStatement(insertSQL);

			pstmt.setString(1, loginId);
			pstmt.setString(2, name);
			pstmt.setString(3, birthDate);
			pstmt.setString(4,password);


			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public User findById(String loginId) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			String sql="SELECT * FROM user WHERE login_id = ?";
			PreparedStatement pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, loginId);
			ResultSet rs=pstmt.executeQuery(sql);

			if(!rs.next()) {
				return null;
			}

			String loginIdData=rs.getString("login_id");
			return new User(loginIdData);


		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;

	}

	public List<User> serch(String loginId, String name, String birth, String birthB) {
		List<User> serchList=new ArrayList<User>();
		Connection conn = null;
		conn = DBManager.getConnection();
		try {
			String sql="SELECT * FROM user WHERE login_id !='admin'";

			if(!loginId.equals("")) {
				sql+="AND login_id ='" + loginId + "'";
			}
			if(!name.equals("")) {
				sql+="AND name LIKE '%" + name + "%'";
			}
			if(!birth.equals("")) {
				sql+="AND birth_date >= "+birth+" ";
			}
			if(!birthB.equals("")) {
				sql+="AND birth_date <= "+birthB+" ";
			}

			PreparedStatement pStmt=conn.prepareStatement(sql);
			System.out.println(sql);
			ResultSet rs=pStmt.executeQuery();

			while(rs.next()) {
				int ID=rs.getInt("id");
				String loginID=rs.getString("login_id");
				String naMe=rs.getString("name");
				Date birthDate=rs.getDate("birth_date");
				User user=new User(ID,loginID,naMe,birthDate);

				serchList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return serchList;
	}

	public void userUpdate(String password,String name,String birthDate,String id) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="UPDATE user SET name=?,birth_date=?,password=?,update_date=now() WHERE id=?";
			pstmt=conn.prepareStatement(insertSQL);



			pstmt.setString(1, name);
			pstmt.setString(2, birthDate);
			pstmt.setString(3,password);
			pstmt.setString(4, id);

			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public void userUpdate2(String name,String birthDate,String id) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="UPDATE user SET name=?,birth_date=?,update_date=now() WHERE id=?";
			pstmt=conn.prepareStatement(insertSQL);


			pstmt.setString(1, name);
			pstmt.setString(2, birthDate);
			pstmt.setString(3, id);

			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public void userDelete(String id) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="DELETE FROM user WHERE id=?";
			pstmt=conn.prepareStatement(insertSQL);

			pstmt.setString(1,id);

			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}
}
